-- DROP FUNCTION service.scappt_filter();

CREATE OR REPLACE FUNCTION service.scappt_filter()
RETURNS trigger AS

$BODY$
/* ***************************************************************   
   DESC:     Uses regex_replace() on scappt.comment column to avoid UTF8 SQL errors in Omnis7.
   PARMS:    none
   RETURNS:  nothing
   USED BY:  SCHED
   MODIFIED: 180406 jeb: SCHED-65 - Created.
             180416 jeb: Added refinements to POSIX arguments.
			 180417 jeb: Testing, fixes & refinements. Version 1 complete.
   DOCUMENT: <DOCUMENTATION><FAMILIES>servlink</FAMILIES> 
             <VERSION>1.00<POINT>1.00</POINT></VERSION>
             <SCRIPT>1018</SCRIPT><URL></URL>
             <KEYS>Sched</KEYS>
             </DOCUMENTATION>
****************************************************************** */

BEGIN
IF TG_OP IN ('UPDATE','INSERT') THEN
     NEW.comment  := regexp_replace(NEW.comment, '[^[:ascii:]]', '','g'); -- this removes all ascii chars w/value > 127 decimal.
	 --                                          '[^a-zA-Z0-9\s\.?&{}"@=+_,:;! *()<>/%$*-]' -- more verbose (& less complete!) version
	RETURN NEW;
END IF;
END;

$BODY$

LANGUAGE plpgsql VOLATILE
COST 100;
  
ALTER FUNCTION service.scappt_filter()
  OWNER TO lyadmin;
  
GRANT EXECUTE ON FUNCTION service.scappt_filter() TO lyadmin;
GRANT EXECUTE ON FUNCTION service.scappt_filter() TO public;
GRANT EXECUTE ON FUNCTION service.scappt_filter() TO lyuser;

CREATE TRIGGER tr_scappt_filter
  BEFORE INSERT OR UPDATE
  ON public.scappt
  FOR EACH ROW
  EXECUTE PROCEDURE service.scappt_filter();

 /* ***********************************************************************
 If the script installed successfully, write the log entry to appversion.
**************************************************************************  */  
INSERT INTO main.appversion (appname,scriptname,appversion,dbversion)
(SELECT nam,scrname,pointv,dbv
 FROM (SELECT 'sched'::varchar AS nam,'Sched-1018-01_JEB-create_trigger_scappt_filter.sql'::varchar AS scrname,'1018.000.000'::varchar AS pointv, 1018 AS dbv) a
 WHERE NOT EXISTS (SELECT 1 FROM main.appversion WHERE scriptname = scrname AND dbversion = dbv)); 
 